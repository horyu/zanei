fn main() {
    if cfg!(target_os = "windows") {
        println!("cargo:rustc-link-arg=/STACK:{}", 2usize.pow(20) * 2);

        let mut res = winres::WindowsResource::new();
        res.set_icon("img/zanei.ico");
        res.compile().unwrap();
    }
}
