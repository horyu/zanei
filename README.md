# Zan'ei | 残影

[TOC]

![capture](img/capture.png)

![simple_diagram](img/simple_diagram.png)

## Japanese | 日本語

Zan'ei（残影）は個人的な目的のために作成したキーボード表示ソフトです。1/60秒の倍数時間が経過するたびに、その間にあった入力の変化を2/60秒以上表示します。

起動時、第1引数で渡されるYAMLファイルか作業ディレクトリの config.yaml を読み込みます。
起動に失敗したら、作業ディレクトリに生成される zanei-error.log が参考になるかもしれません。

[OBS Studio](https://obsproject.com/ja)のウィンドウキャプチャで正常にキャプチャされない場合は、 `キャプチャ方法` を `Windows 10 (1903以降)` に変更してください。

### 実行環境

- OS: Windows10以上
- ディスプレイ: リフレッシュレート60Hz以上

### サポート外

- 特殊キー
- マウス
- 1つのvkCodeに複数の図形を割り当て
- 四角形と文字以外の表示

## English | 英語

Zan'ei is a keyboard visualizer of horyu, by horyu, for horyu. Each time a multiple of 1/60th of a second elapses, Zan'ei displays the change in input that has occurred during that time, over 2/60th of a second.

At startup, Zan'ei reads the YAML file passed as the first argument or config.yaml in the working directory.
If startup fails, zanei-error.log generated in the working directory may be helpful.

If the window capture of [OBS Studio](https://obsproject.com/) does not capture correctly, please change `Capture Method` to `Windows 10 (1903 and up)`.

### Runtime Environment

- OS: Windows 10 or higher
- Display: Refresh rate 60Hz or higher

### Not supported

- Special keys
- Mouse
- Assigning multiple shapes to a single vkCode
- Displaying other than squares and text

## License | ライセンス

See [LICENSE](./LICENSE).

This product uses [Microsoft](https://github.com/microsoft)/[windows-rs](https://github.com/microsoft/windows-rs).
