// reference
// https://github.com/microsoft/windows-rs/blob/d40a51812f0a943f1c2124948ef4436d9276dbf4/crates/samples/direct2d/src/main.rs
use std::sync::atomic::{AtomicBool, Ordering::Relaxed};

use windows::{
    core::*, Foundation::Numerics::*, Win32::Foundation::*, Win32::Graphics::Direct2D::Common::*,
    Win32::Graphics::Direct2D::*, Win32::Graphics::Direct3D::*, Win32::Graphics::Direct3D11::*,
    Win32::Graphics::Dxgi::Common::*, Win32::Graphics::Dxgi::*,
};

use crate::config::RenderConfig;
use crate::input_observer::InputObserver;

static STOP_RENDERER_FLAG: AtomicBool = AtomicBool::new(false);

pub fn set_true_to_stop_renderer_flag() {
    STOP_RENDERER_FLAG.store(true, Relaxed);
}

fn should_stop_renderer() -> bool {
    STOP_RENDERER_FLAG.load(Relaxed)
}

pub struct Renderer {
    device_context: ID2D1DeviceContext,
    dxgi_swapchain: IDXGISwapChain1,
    solid_color_brush: ID2D1SolidColorBrush,

    render_config: RenderConfig,
    input_observer: InputObserver,
}

impl Renderer {
    pub fn new(
        handle: HWND,
        render_config: RenderConfig,
        input_observer: InputObserver,
    ) -> Result<Self> {
        let factory = create_factory()?;
        let mut dpix = 0.0;
        let mut dpiy = 0.0;
        unsafe { factory.GetDesktopDpi(&mut dpix, &mut dpiy) };

        let device = create_device()?;
        let device_context = create_device_context(&factory, &device)?;
        unsafe { device_context.SetDpi(dpix, dpiy) };

        let dxgi_swapchain = create_swapchain(&device, handle)?;
        create_swapchain_bitmap(&dxgi_swapchain, &device_context)?;

        let solid_color_brush = create_solid_color_brush(&device_context)?;

        Ok(Self {
            device_context,
            dxgi_swapchain,
            solid_color_brush,
            render_config,
            input_observer,
        })
    }

    pub fn start(&mut self) -> Result<()> {
        let device_context = &self.device_context;
        let dxgi_swapchain = &self.dxgi_swapchain;

        self.input_observer.set_base_time();

        loop {
            if !self.input_observer.update_inputs() {
                continue;
            }
            if should_stop_renderer() {
                return Ok(());
            }
            unsafe {
                device_context.BeginDraw();
                self.draw(device_context);
                device_context.EndDraw(std::ptr::null_mut(), std::ptr::null_mut())?;
                match dxgi_swapchain.Present(0, 0).ok() {
                    Ok(_) => (),
                    err => return err,
                }
            }
        }
    }

    fn draw(&self, device_context: &ID2D1DeviceContext) {
        let brush = &self.solid_color_brush;
        unsafe {
            device_context.Clear(&self.render_config.background_color);

            for &vk_code in &self.render_config.vk_code_orders {
                let vk_code_config = self.render_config.vk_code_configs[vk_code]
                    .as_ref()
                    .unwrap();
                let pre_states = &self.input_observer.pre_states[vk_code];
                let latest_states = &self.input_observer.latest_states[vk_code];

                let base_rect = vk_code_config.base_rect;
                let D2D_RECT_F { top, bottom, .. } = base_rect;
                let mid = (top + bottom) / 2.0;
                let mut rect = base_rect;

                let inverse_pre_len = 1.0 / (pre_states.len() as f32);
                rect.top = bottom.ceil();
                for i in 0..=pre_states.pos {
                    let state = pre_states.get(i);
                    let j = (i + 1) as f32;
                    rect.bottom = rect.top;
                    rect.top =
                        ((1.0 - j * inverse_pre_len) * bottom + (j * inverse_pre_len) * mid).ceil();

                    let background_color = if state {
                        &vk_code_config.background_on_color
                    } else {
                        &vk_code_config.background_off_color
                    };
                    brush.SetColor(background_color);
                    device_context.FillRectangle(&rect, brush);
                }

                let inverse_latest_len = 1.0 / (latest_states.len() as f32);
                for i in 0..=latest_states.pos {
                    let state = latest_states.get(i);
                    let j = (i + 1) as f32;
                    rect.bottom = rect.top;
                    rect.top = ((1.0 - j * inverse_latest_len) * mid
                        + (j * inverse_latest_len) * top)
                        .ceil();

                    let background_color = if state {
                        &vk_code_config.background_on_color
                    } else {
                        &vk_code_config.background_off_color
                    };
                    brush.SetColor(background_color);
                    device_context.FillRectangle(&rect, brush);
                }

                let text_color = if latest_states.last() {
                    &vk_code_config.text_on_color
                } else {
                    &vk_code_config.text_off_color
                };
                brush.SetColor(text_color);
                device_context.DrawTextLayout(
                    D2D_POINT_2F {
                        x: vk_code_config.base_rect.left,
                        y: vk_code_config.base_rect.top,
                    },
                    &vk_code_config.text_off_layout,
                    brush,
                    D2D1_DRAW_TEXT_OPTIONS_NONE,
                );
            }
        }
    }
}

fn create_factory() -> Result<ID2D1Factory1> {
    let debug_level = if cfg!(debug_assertions) {
        D2D1_DEBUG_LEVEL_INFORMATION
    } else {
        D2D1_DEBUG_LEVEL_NONE
    };
    let options = D2D1_FACTORY_OPTIONS {
        debugLevel: debug_level,
    };
    unsafe { D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &options) }
}

fn create_device() -> Result<ID3D11Device> {
    let mut result = create_device_with_type(D3D_DRIVER_TYPE_HARDWARE);
    if let Err(err) = &result {
        if err.code() == DXGI_ERROR_UNSUPPORTED {
            result = create_device_with_type(D3D_DRIVER_TYPE_WARP);
        }
    }
    result
}

fn create_device_with_type(drive_type: D3D_DRIVER_TYPE) -> Result<ID3D11Device> {
    let mut flags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;
    if cfg!(debug_assertions) {
        flags |= D3D11_CREATE_DEVICE_DEBUG;
    }

    let mut device = None;
    unsafe {
        D3D11CreateDevice(
            None,
            drive_type,
            HINSTANCE::default(),
            flags,
            &[],
            D3D11_SDK_VERSION,
            &mut device,
            std::ptr::null_mut(),
            &mut None,
        )
        .map(|()| device.unwrap())
    }
}

fn create_device_context(
    factory: &ID2D1Factory1,
    device: &ID3D11Device,
) -> Result<ID2D1DeviceContext> {
    unsafe {
        let d2device = factory.CreateDevice(&device.cast::<IDXGIDevice>()?)?;
        let target = d2device.CreateDeviceContext(D2D1_DEVICE_CONTEXT_OPTIONS_NONE)?;

        target.SetUnitMode(D2D1_UNIT_MODE_DIPS);

        Ok(target)
    }
}

fn create_swapchain(device: &ID3D11Device, window: HWND) -> Result<IDXGISwapChain1> {
    let dxgi_factory = get_dxgi_factory(device)?;

    let props = DXGI_SWAP_CHAIN_DESC1 {
        Format: DXGI_FORMAT_B8G8R8A8_UNORM,
        SampleDesc: DXGI_SAMPLE_DESC {
            Count: 1,
            Quality: 0,
        },
        BufferUsage: DXGI_USAGE_RENDER_TARGET_OUTPUT,
        BufferCount: 2,
        SwapEffect: DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL,
        ..Default::default()
    };

    unsafe { dxgi_factory.CreateSwapChainForHwnd(device, window, &props, std::ptr::null(), None) }
}

fn get_dxgi_factory(device: &ID3D11Device) -> Result<IDXGIFactory2> {
    let dxgi_device = device.cast::<IDXGIDevice>()?;
    unsafe { dxgi_device.GetAdapter()?.GetParent() }
}

fn create_swapchain_bitmap(
    dxgi_swapchain: &IDXGISwapChain1,
    device_context: &ID2D1DeviceContext,
) -> Result<()> {
    let dxgi_surface: IDXGISurface = unsafe { dxgi_swapchain.GetBuffer(0)? };

    let bitmap_propeties = D2D1_BITMAP_PROPERTIES1 {
        pixelFormat: D2D1_PIXEL_FORMAT {
            format: DXGI_FORMAT_B8G8R8A8_UNORM,
            alphaMode: D2D1_ALPHA_MODE_IGNORE,
        },
        dpiX: 96.0,
        dpiY: 96.0,
        bitmapOptions: D2D1_BITMAP_OPTIONS_TARGET | D2D1_BITMAP_OPTIONS_CANNOT_DRAW,
        colorContext: None,
    };

    unsafe {
        let bitmap =
            device_context.CreateBitmapFromDxgiSurface(&dxgi_surface, &bitmap_propeties)?;
        device_context.SetTarget(&bitmap);
    };

    Ok(())
}

fn create_solid_color_brush(target: &ID2D1DeviceContext) -> Result<ID2D1SolidColorBrush> {
    let brush_properties = D2D1_BRUSH_PROPERTIES {
        opacity: 1.0,
        transform: Matrix3x2::identity(),
    };

    unsafe { target.CreateSolidColorBrush(&D2D1_COLOR_F::default(), &brush_properties) }
}
