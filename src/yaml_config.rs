mod color;
mod defalut_vk_code;
mod font;
mod vk_code;
mod window;

pub use color::YamlColor;
pub use defalut_vk_code::YamlDefaultVkCode;
pub use font::YamlFont;
pub use vk_code::YamlVkCode;
pub use window::YamlWindow;

use std::collections::HashMap;

use serde::Deserialize;
use validator::Validate;

#[derive(Debug, Deserialize, Validate)]
pub struct YamlConfig {
    #[validate]
    pub window: YamlWindow,
    #[validate]
    #[serde(rename = "defaultVkCode")]
    pub default_vk_code: YamlDefaultVkCode,
    // To show key(u8), vk_code doesn't use varidate attribute.
    #[serde(rename = "vkCode")]
    pub vk_code: HashMap<u8, YamlVkCode>,
}

impl YamlConfig {
    pub fn validation_error_strings(&self) -> Option<Vec<String>> {
        let mut error_strings = vec![];

        if let Err(errors) = self.validate() {
            for (&key, e) in errors.errors() {
                error_strings.push(format!("{key}: {e:#?}"));
            }
        }

        for (vk_code_key, vk_code) in &self.vk_code {
            if let Err(e) = vk_code.validate() {
                error_strings.push(format!("vkCode.{vk_code_key}: {e:#?}"));
            }
        }

        if error_strings.is_empty() {
            None
        } else {
            Some(error_strings)
        }
    }
}

#[cfg(test)]
#[macro_export]
macro_rules! validate_ok {
    ($yaml_data:expr) => {
        assert!(
            $yaml_data.validate().is_ok(),
            "{:#?}",
            $yaml_data.validate()
        );
    };
}

#[cfg(test)]
#[macro_export]
macro_rules! validate_err {
    ($yaml_data:expr) => {
        assert!($yaml_data.validate().is_err());
    };
}

#[cfg(test)]
mod tests {
    use super::*;

    fn parse(s: &str) -> YamlConfig {
        serde_yaml::from_str::<YamlConfig>(s).unwrap()
    }

    #[test]
    fn ok() {
        let c = parse(&format!(
            "
        colors:
            - {}
        fonts:
            - {}
        window: {}
        defaultVkCode: {}
        vkCode:
            63: {}
        ",
            YamlColor::default_oneline_string(),
            YamlFont::default_oneline_string(),
            YamlWindow::default_oneline_string(),
            YamlDefaultVkCode::default_oneline_string(),
            YamlVkCode::default_oneline_string(),
        ));
        validate_ok!(c);

        let c = parse(&format!(
            "
        colors:
            - {}
        fonts:
            - {}
        window: {}
        defaultVkCode: {}
        vkCode:
            63: {}
            64:
                text: b
                left: 0.0
                top: 0.0
                backgroundColor:
                    rgb: invalid_data_in_vkCode
        ",
            YamlColor::default_oneline_string(),
            YamlFont::default_oneline_string(),
            YamlWindow::default_oneline_string(),
            YamlDefaultVkCode::default_oneline_string(),
            YamlVkCode::default_oneline_string(),
        ));
        validate_ok!(c);
    }
}
