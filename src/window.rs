// reference
// https://github.com/microsoft/windows-rs/blob/d40a51812f0a943f1c2124948ef4436d9276dbf4/crates/samples/direct2d/src/main.rs
use windows::{
    core::*, Win32::Foundation::*, Win32::System::LibraryLoader::*,
    Win32::UI::WindowsAndMessaging::*,
};

use crate::config::{RenderConfig, WindowConfig};
use crate::input_observer::InputObserver;
use crate::renderer::{set_true_to_stop_renderer_flag, Renderer};

pub struct Window {
    handle: HWND,
}

impl Window {
    pub fn new() -> Self {
        Self { handle: HWND(0) }
    }

    pub fn run(
        &mut self,
        window_config: WindowConfig,
        render_config: RenderConfig,
        input_observer: InputObserver,
    ) -> Result<()> {
        unsafe {
            let instance = GetModuleHandleA(None)?;
            debug_assert!(instance.0 != 0);

            let wc = WNDCLASSA {
                hInstance: instance,
                lpszClassName: PCSTR(b"zanei\0".as_ptr()),
                lpfnWndProc: Some(Self::wndproc),
                ..Default::default()
            };

            let atom = RegisterClassA(&wc);
            debug_assert!(atom != 0);

            let handle = CreateWindowExA(
                Default::default(),
                PCSTR(b"zanei\0".as_ptr()),
                PCSTR(b"Zanei\0".as_ptr()),
                WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_VISIBLE, // https://docs.microsoft.com/ja-jp/windows/win32/winmsg/window-styles
                window_config.x,
                window_config.y,
                window_config.width,
                window_config.height,
                None,
                None,
                instance,
                self as *mut _ as _,
            );

            debug_assert!(handle.0 != 0);
            debug_assert!(handle == self.handle);

            let join_handle = std::thread::spawn(move || {
                let mut renderer = Renderer::new(handle, render_config, input_observer)?;
                renderer.start()
            });

            let mut message = MSG::default();
            let hwnd_default = HWND::default();
            let mut is_thread_finished = false;

            loop {
                // https://learn.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getmessagea#return-value
                match GetMessageA(&mut message, hwnd_default, 0, 0).0 {
                    0 => {
                        // WM_QUIT
                        if join_handle.is_finished() {
                            return join_handle.join().unwrap();
                        }
                        return Ok(());
                    }
                    -1 => {
                        // error
                        if join_handle.is_finished() {
                            match join_handle.join().unwrap() {
                                Ok(_) => (),
                                err => return err,
                            }
                        }
                        return Err(::windows::core::Error::from_win32());
                    }
                    _ => {
                        DispatchMessageA(&message);
                    }
                }
                if !is_thread_finished && join_handle.is_finished() {
                    PostQuitMessage(0);
                    is_thread_finished = true;
                }
            }
        }
    }

    extern "system" fn wndproc(
        window: HWND,
        message: u32,
        wparam: WPARAM,
        lparam: LPARAM,
    ) -> LRESULT {
        unsafe {
            if message == WM_NCCREATE {
                let cs = lparam.0 as *const CREATESTRUCTA;
                let this = (*cs).lpCreateParams as *mut Self;
                (*this).handle = window;

                SetWindowLongPtrA(window, GWLP_USERDATA, this as isize);
            } else {
                let this = GetWindowLongPtrA(window, GWLP_USERDATA) as *mut Self;

                if !this.is_null() {
                    return (*this).message_handler(message, wparam, lparam);
                }
            }

            DefWindowProcA(window, message, wparam, lparam)
        }
    }

    fn message_handler(&mut self, message: u32, wparam: WPARAM, lparam: LPARAM) -> LRESULT {
        unsafe {
            match message {
                WM_DESTROY => {
                    set_true_to_stop_renderer_flag();
                    PostQuitMessage(0);
                    LRESULT(0)
                }
                _ => DefWindowProcA(self.handle, message, wparam, lparam),
            }
        }
    }
}
