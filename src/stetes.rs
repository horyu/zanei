#[derive(Debug, Copy, Clone)]
pub struct States {
    pub pos: u8,
    data: u8,
}

impl States {
    pub fn new() -> Self {
        Self { pos: 0, data: 0 }
    }

    pub fn reset_by(&mut self, new_state: bool) {
        self.pos = 0;
        self.data = new_state as u8;
    }

    pub fn get(&self, index: u8) -> bool {
        (self.data >> index) & 1 != 0
    }

    pub fn last(&self) -> bool {
        self.get(self.pos)
    }

    pub fn push(&mut self, state: bool) {
        self.pos += 1;
        self.data |= (state as u8) << self.pos;
    }

    pub fn len(&self) -> u8 {
        self.pos + 1
    }
}
