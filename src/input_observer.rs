use std::{collections::VecDeque, sync::Mutex};

use anyhow::{bail, Result};
use once_cell::sync::Lazy;
use windows_sys::{
    Win32::Foundation::*, Win32::System::SystemInformation::*, Win32::UI::WindowsAndMessaging::*,
};

use crate::{stetes::States, vk_code_array::*};

/// 100ns unit
const FRAME_TIME: u64 = 1_000_000_000 / 100 / 60;
/// 1_000_000_000 / 100 / 60 = 1_000_000 / 6 = 500_000 / 3
const FRAME_NUMERATOR: u64 = 500_000;
/// 1_000_000_000 / 100 / 60 = 1_000_000 / 6 = 500_000 / 3
const FRAME_DENOMINATOR: u64 = 3;

struct Input {
    vk_code: usize,
    state: bool,
    time: u64,
}

static mut INPUTS_QUEUE: Lazy<Mutex<VecDeque<Input>>> = Lazy::new(|| Mutex::new(VecDeque::new()));

static mut IS_TARGET_VK_CODE: VkCodeArray<bool> = [false; VK_CODE_SIZE];

static mut CURRENT_VK_CODE_STATE: VkCodeArray<bool> = [false; VK_CODE_SIZE];

static mut H_HOOK: HHOOK = 0;

pub struct InputObserver {
    vk_codes: Vec<usize>,
    pub latest_states: VkCodeArray<States>,
    pub pre_states: VkCodeArray<States>,
    base_time: u64,
    latest_time: u64,
    frames: u64,
}

impl InputObserver {
    pub fn new(vk_codes: &[usize]) -> Result<Self> {
        unsafe {
            H_HOOK = SetWindowsHookExA(
                WH_KEYBOARD_LL,
                Some(low_level_keyboard_proc),
                std::ptr::null_mut::<std::ffi::c_void>() as isize,
                0,
            );
            // dbg!(H_HOOK);
            if H_HOOK == std::ptr::null_mut::<std::ffi::c_void>() as isize {
                bail!("Failed to SetWindowsHookExA");
            }
        };

        let vk_codes = vk_codes.to_owned();

        for &vk_code in &vk_codes {
            unsafe {
                IS_TARGET_VK_CODE[vk_code] = true;
            }
        }

        Ok(Self {
            vk_codes,
            latest_states: [States::new(); VK_CODE_SIZE],
            pre_states: [States::new(); VK_CODE_SIZE],
            base_time: 0,
            latest_time: 0,
            frames: 0,
        })
    }

    pub fn set_base_time(&mut self) {
        let time = get_system_time_precise();
        self.base_time = time;
        self.latest_time = time;
        unsafe {
            INPUTS_QUEUE.get_mut().unwrap().clear();
        }
    }

    /// Return true when there is an update
    pub fn update_inputs(&mut self) -> bool {
        let base_time = self.base_time;
        let diff_time = get_system_time_precise() - self.latest_time;
        let diff_frame_count = diff_time * FRAME_DENOMINATOR / FRAME_NUMERATOR;

        match diff_frame_count.cmp(&1) {
            std::cmp::Ordering::Less => return false,
            std::cmp::Ordering::Equal => {
                std::mem::swap(&mut self.latest_states, &mut self.pre_states);
                // initialize latest states with previous states
                for &vk_code in &self.vk_codes {
                    let last_state = self.pre_states[vk_code].last();
                    self.latest_states[vk_code].reset_by(last_state);
                }

                let time_just = base_time
                    + (self.frames + diff_frame_count) * FRAME_NUMERATOR / FRAME_DENOMINATOR;

                let inputs_queque = unsafe { INPUTS_QUEUE.get_mut().unwrap() };
                while !inputs_queque.is_empty() && inputs_queque[0].time <= time_just {
                    if let Some(input) = inputs_queque.pop_front() {
                        let pre_state = self.latest_states[input.vk_code].last();
                        if pre_state != input.state {
                            self.latest_states[input.vk_code].push(input.state);
                        }
                    }
                }

                self.latest_time = time_just;
                self.frames += diff_frame_count;
            }
            std::cmp::Ordering::Greater => {
                let time_just = base_time
                    + (self.frames + diff_frame_count) * FRAME_NUMERATOR / FRAME_DENOMINATOR;
                let time_1f_ago = time_just - FRAME_TIME;
                let time_2f_ago = time_1f_ago - FRAME_TIME;

                let mut target_inputs = unsafe {
                    let mut vd = VecDeque::new();
                    let inputs_queque = INPUTS_QUEUE.get_mut().unwrap();
                    while !inputs_queque.is_empty() && inputs_queque[0].time <= time_just {
                        if let Some(input) = inputs_queque.pop_front() {
                            vd.push_back(input);
                        }
                    }
                    vd
                };

                // find states 2 frames ago
                let mut states = [false; VK_CODE_SIZE];
                for &vk_code in &self.vk_codes {
                    states[vk_code] = self.latest_states[vk_code].last();
                }
                while !target_inputs.is_empty() && target_inputs[0].time <= time_2f_ago {
                    if let Some(input) = target_inputs.pop_front() {
                        states[input.vk_code] = input.state;
                    }
                }

                // update pre_states with states and target_inputs
                for &vk_code in &self.vk_codes {
                    self.pre_states[vk_code].reset_by(states[vk_code]);
                }
                while !target_inputs.is_empty() && target_inputs[0].time <= time_1f_ago {
                    if let Some(input) = target_inputs.pop_front() {
                        let last_state = states.get_mut(input.vk_code).unwrap();
                        if *last_state != input.state {
                            *last_state = input.state;
                            self.pre_states[input.vk_code].push(input.state);
                        }
                    }
                }

                // update latest_states with pre_inputs and target_inputs
                for &vk_code in &self.vk_codes {
                    self.latest_states[vk_code].reset_by(states[vk_code]);
                }
                while let Some(input) = target_inputs.pop_front() {
                    let last_state = states.get_mut(input.vk_code).unwrap();
                    if *last_state != input.state {
                        *last_state = input.state;
                        self.latest_states[input.vk_code].push(input.state);
                    }
                }

                self.latest_time = time_just;
                self.frames += diff_frame_count;
            }
        }

        true
    }
}

extern "system" fn low_level_keyboard_proc(ncode: i32, wparam: WPARAM, lparam: LPARAM) -> LRESULT {
    unsafe {
        if ncode == HC_ACTION as i32 {
            match wparam as u32 {
                WM_KEYDOWN => {
                    let kbd_ll_hook_struct: &KBDLLHOOKSTRUCT = std::mem::transmute(lparam);
                    let vk_code = kbd_ll_hook_struct.vkCode as usize;
                    // eprintln!("T: {vk_code} {vk_code:x}");
                    // eprintln!(
                    //     "T: {vk_code} {vk_code:x}: {} {:x} {}",
                    //     kbd_ll_hook_struct.scanCode,
                    //     kbd_ll_hook_struct.scanCode,
                    //     kbd_ll_hook_struct.time
                    // );
                    if IS_TARGET_VK_CODE[vk_code] && !CURRENT_VK_CODE_STATE[vk_code] {
                        CURRENT_VK_CODE_STATE[vk_code] = true;
                        let input = Input {
                            vk_code,
                            state: true,
                            time: get_system_time_precise(),
                        };
                        INPUTS_QUEUE.get_mut().unwrap().push_back(input);
                    }
                }
                WM_KEYUP => {
                    let kbd_ll_hook_struct: &KBDLLHOOKSTRUCT = std::mem::transmute(lparam);
                    let vk_code = kbd_ll_hook_struct.vkCode as usize;
                    // eprintln!(
                    //     "F: {vk_code} {vk_code:x}: {} {:x} {}",
                    //     kbd_ll_hook_struct.scanCode,
                    //     kbd_ll_hook_struct.scanCode,
                    //     kbd_ll_hook_struct.time
                    // );
                    if IS_TARGET_VK_CODE[vk_code] {
                        CURRENT_VK_CODE_STATE[vk_code] = false;
                        let input = Input {
                            vk_code,
                            state: false,
                            time: get_system_time_precise(),
                        };
                        INPUTS_QUEUE.get_mut().unwrap().push_back(input);
                    }
                }
                _ => (),
            }
        }
        CallNextHookEx(H_HOOK, ncode, wparam, lparam)
    }
}

fn get_system_time_precise() -> u64 {
    unsafe {
        let mut filetime = std::mem::MaybeUninit::<FILETIME>::uninit();
        GetSystemTimePreciseAsFileTime(filetime.as_mut_ptr());
        std::mem::transmute(filetime.assume_init())
    }
}
