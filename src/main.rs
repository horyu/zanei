#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]
mod config;
mod input_observer;
mod renderer;
mod stetes;
mod vk_code_array;
mod window;
mod yaml_config;

use anyhow::{bail, Context, Result};

use config::Config;
use input_observer::InputObserver;
use window::Window;
use yaml_config::YamlConfig;

use std::{ffi::OsString, fs::File, io::Write, path::Path};

fn main() {
    if let Err(err) = try_main() {
        // eprintln!("{:#?}", err);
        // eprintln!("{}", err);
        dbg!(&err);
        if let Ok(mut f) = File::create("zanei-error.log") {
            let _ = f.write_fmt(format_args!("{err:#?}"));
        }
        std::process::exit(1);
    }
}

fn try_main() -> Result<()> {
    let Config {
        window_config,
        render_config,
    } = make_config()?;
    let input_observer = InputObserver::new(&render_config.vk_code_orders)?;
    let mut window = Window::new();
    window
        .run(window_config, render_config, input_observer)
        .context("Window error")
}

fn make_config() -> Result<Config> {
    let args: Vec<OsString> = std::env::args_os().collect();
    let config_yaml_path = if 2 <= args.len() {
        Path::new(&args[1])
    } else {
        Path::new("config.yaml")
    };

    let yaml_config = read_yaml_config(config_yaml_path)?;
    if let Some(validation_errors) = yaml_config.validation_error_strings() {
        bail!(validation_errors.join("\n"));
    }

    Config::new(&yaml_config).with_context(|| "Failed to make config.")
}

fn read_yaml_config(path: &Path) -> Result<YamlConfig> {
    if !matches!(path.try_exists(), Ok(true)) {
        bail!(r#""{}" is not found."#, path.to_string_lossy());
    }
    if !path.is_file() {
        bail!(r#""{}" is not file."#, path.to_string_lossy());
    }

    let buf = std::fs::read(path)
        .with_context(|| format!(r#"Failed to read from "{}"."#, path.display()))?;

    serde_yaml::from_slice::<YamlConfig>(&buf).with_context(|| "Failed to parse file as YAML.")
}
