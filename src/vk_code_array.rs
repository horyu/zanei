pub use array_macro::array;

pub const VK_CODE_SIZE: usize = 256;
pub type VkCodeArray<T> = [T; VK_CODE_SIZE];
