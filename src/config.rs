use crate::vk_code_array::*;
use crate::yaml_config::{
    YamlColor, YamlConfig, YamlDefaultVkCode, YamlFont, YamlVkCode, YamlWindow,
};

use windows::{
    core::*, Win32::Graphics::Direct2D::Common::*, Win32::Graphics::DirectWrite::*,
    Win32::UI::WindowsAndMessaging::*,
};

#[derive(Clone, Debug)]
pub struct Config {
    pub window_config: WindowConfig,
    pub render_config: RenderConfig,
}

#[derive(Clone, Debug)]
pub struct WindowConfig {
    pub x: i32,
    pub y: i32,
    pub width: i32,
    pub height: i32,
}

#[derive(Clone, Debug)]
pub struct RenderConfig {
    pub background_color: D2D1_COLOR_F,
    pub vk_code_configs: VkCodeArray<Option<VkCodeConfig>>,
    pub vk_code_orders: Vec<usize>,
}

unsafe impl Send for RenderConfig {}

#[derive(Clone, Debug)]
pub struct VkCodeConfig {
    pub base_rect: D2D_RECT_F,
    pub background_off_color: D2D1_COLOR_F,
    pub background_on_color: D2D1_COLOR_F,
    pub text_off_color: D2D1_COLOR_F,
    pub text_on_color: D2D1_COLOR_F,
    pub text_off_layout: IDWriteTextLayout,
    pub text_on_layout: IDWriteTextLayout,
}

impl Config {
    pub fn new(yaml_config: &YamlConfig) -> Result<Config> {
        let window_config = create_window_config(yaml_config);
        let render_config = create_render_config(yaml_config)?;

        Ok(Config {
            window_config,
            render_config,
        })
    }
}

fn create_window_config(yaml_config: &YamlConfig) -> WindowConfig {
    let YamlWindow {
        x,
        y,
        width,
        height,
        ..
    } = &yaml_config.window;

    WindowConfig {
        x: x.unwrap_or(CW_USEDEFAULT),
        y: y.unwrap_or(CW_USEDEFAULT),
        width: width.unwrap_or(CW_USEDEFAULT),
        height: height.unwrap_or(CW_USEDEFAULT),
    }
}

fn create_render_config(yaml_config: &YamlConfig) -> Result<RenderConfig> {
    let write_factory: IDWriteFactory = unsafe {
        std::mem::transmute(DWriteCreateFactory(
            DWRITE_FACTORY_TYPE_SHARED,
            &IDWriteFactory::IID,
        )?)
    };
    let font_collection: IDWriteFontCollection = unsafe {
        let mut opt = None;
        write_factory
            .GetSystemFontCollection(&mut opt, false)
            .map(|_| opt.unwrap())?
    };

    let YamlDefaultVkCode {
        width: defualt_width,
        height: defualt_height,
        background_color: default_background_color,
        background_on_color_opt: default_background_on_color_opt,
        font: default_font,
        on_font_opt: default_on_font_opt,
        text_color: default_text_color,
        text_on_color_opt: default_text_on_color_opt,
    } = &yaml_config.default_vk_code;

    let mut vk_code_configs = array![None; VK_CODE_SIZE];
    let mut vk_code_z_index_pairs = vec![];

    for (vk_code, yaml_vk_code) in &yaml_config.vk_code {
        let vk_code = *vk_code as usize;
        let YamlVkCode {
            text,
            left,
            top,
            right_opt,
            bottom_opt,
            background_color_opt,
            background_on_color_opt,
            text_color_opt,
            text_on_color_opt,
            font_opt,
            on_font_opt,
            z_index_opt,
        } = yaml_vk_code;

        let left = *left;
        let top = *top;
        let right = right_opt.unwrap_or(left + defualt_width);
        let bottom = bottom_opt.unwrap_or(top + defualt_height);
        let base_rect = D2D_RECT_F {
            left,
            top,
            right,
            bottom,
        };

        let background_off_color = background_color_opt
            .as_ref()
            .unwrap_or(default_background_color);
        let background_on_color = background_on_color_opt
            .as_ref()
            .or(default_background_on_color_opt.as_ref())
            .unwrap_or(background_off_color);

        let text_off_color = text_color_opt.as_ref().unwrap_or(default_text_color);
        let text_on_color = text_on_color_opt
            .as_ref()
            .or(default_text_on_color_opt.as_ref())
            .unwrap_or(text_off_color);

        let off_font = font_opt.as_ref().unwrap_or(default_font);
        let text_off_format = create_text_format(&write_factory, &font_collection, off_font)?;
        let text_off_layout = create_text_layout(
            &write_factory,
            &text_off_format,
            text,
            right - left,
            bottom - top,
        )?;
        let on_font = on_font_opt
            .as_ref()
            .or(default_on_font_opt.as_ref())
            .unwrap_or(off_font);
        let text_on_format = create_text_format(&write_factory, &font_collection, on_font)?;
        let text_on_layout = create_text_layout(
            &write_factory,
            &text_on_format,
            text,
            right - left,
            bottom - top,
        )?;

        let vk_code_config = VkCodeConfig {
            base_rect,
            background_off_color: create_color(background_off_color),
            background_on_color: create_color(background_on_color),
            text_off_color: create_color(text_off_color),
            text_on_color: create_color(text_on_color),
            text_off_layout,
            text_on_layout,
        };

        vk_code_configs[vk_code] = Some(vk_code_config);
        vk_code_z_index_pairs.push((vk_code, z_index_opt.unwrap_or_default()));
    }

    vk_code_z_index_pairs.sort_unstable_by_key(|(_vk_code, z_index)| *z_index);
    let vk_code_orders = vk_code_z_index_pairs
        .into_iter()
        .map(|(vk_code, _z_index)| vk_code)
        .collect();

    Ok(RenderConfig {
        background_color: create_color(&yaml_config.window.background_color),
        vk_code_configs,
        vk_code_orders,
    })
}

fn create_text_layout(
    write_factory: &IDWriteFactory,
    textformat: &IDWriteTextFormat,
    text: &str,
    max_width: f32,
    max_height: f32,
) -> Result<IDWriteTextLayout> {
    let string = text.encode_utf16().chain(Some(0)).collect::<Vec<u16>>();

    unsafe { write_factory.CreateTextLayout(string.as_slice(), textformat, max_width, max_height) }
}

fn create_text_format(
    write_factory: &IDWriteFactory,
    font_collection: &IDWriteFontCollection,
    yaml_font: &YamlFont,
) -> Result<IDWriteTextFormat> {
    unsafe {
        let mut font_name_utf16_bytes: Vec<u16> = yaml_font.font_name.encode_utf16().collect();
        font_name_utf16_bytes.push(0);
        let text_format = write_factory.CreateTextFormat(
            PCWSTR::from_raw(font_name_utf16_bytes.as_ptr()),
            font_collection,
            DWRITE_FONT_WEIGHT(yaml_font.weight),
            DWRITE_FONT_STYLE(yaml_font.style),
            DWRITE_FONT_STRETCH(yaml_font.stretch),
            yaml_font.size,
            PCWSTR::from_raw([0].as_ptr()),
            // PCWSTR::null(),
        )?;
        text_format
            .SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER)
            .and_then(|_| text_format.SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER))
            .map(|_| text_format)
    }
}

fn create_color(yaml_color: &YamlColor) -> D2D1_COLOR_F {
    let (r, g, b) = match (
        &yaml_color.rgb_opt,
        yaml_color.r_opt,
        yaml_color.g_opt,
        yaml_color.b_opt,
    ) {
        (Some(rgb_text), _, _, _) => {
            let len = rgb_text.len();
            debug_assert!(len == 6 || len == 7);
            (
                u32::from_str_radix(&rgb_text[(len - 6)..(len - 4)], 16)
                    .expect("fail u32::from_str_radix") as f32
                    / 255.0,
                u32::from_str_radix(&rgb_text[(len - 4)..(len - 2)], 16)
                    .expect("fail u32::from_str_radix") as f32
                    / 255.0,
                u32::from_str_radix(&rgb_text[(len - 2)..len], 16)
                    .expect("fail u32::from_str_radix") as f32
                    / 255.0,
            )
        }
        (None, Some(r), Some(g), Some(b)) => (r, g, b),
        _ => unreachable!("unexpect yaml_color. is this validated?"),
    };

    let a = yaml_color.a_opt.unwrap_or(1.0);

    D2D1_COLOR_F { r, g, b, a }
}
