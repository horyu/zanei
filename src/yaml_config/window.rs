use super::YamlColor;

use serde::Deserialize;
use validator::Validate;

#[derive(Debug, Deserialize, Validate)]
pub struct YamlWindow {
    pub x: Option<i32>,
    pub y: Option<i32>,
    #[validate(range(min = 0))]
    pub width: Option<i32>,
    #[validate(range(min = 0))]
    pub height: Option<i32>,
    #[validate]
    #[serde(rename = "backgroundColor")]
    pub background_color: YamlColor,
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::validate_ok;

    impl YamlWindow {
        pub fn default_oneline_string() -> String {
            format!(
                "{{backgroundColor: {}}}",
                YamlColor::default_oneline_string(),
            )
        }
    }

    fn parse(s: &str) -> YamlWindow {
        serde_yaml::from_str::<YamlWindow>(s).unwrap()
    }

    #[test]
    fn ok() {
        let w = parse(&YamlWindow::default_oneline_string());
        validate_ok!(w);

        let w = parse(&format!(
            r##"
        x: 0
        y: 0
        width: 0
        height: 0
        backgroundColor: {}
        "##,
            YamlColor::default_oneline_string(),
        ));
        validate_ok!(w);
    }
}
