use super::YamlColor;
use super::YamlFont;

use serde::Deserialize;
use validator::Validate;

#[derive(Debug, Deserialize, Validate)]
pub struct YamlVkCode {
    pub text: String,
    pub left: f32,
    pub top: f32,
    #[serde(rename = "right")]
    pub right_opt: Option<f32>,
    #[serde(rename = "bottom")]
    pub bottom_opt: Option<f32>,
    #[validate]
    #[serde(rename = "backgroundColor")]
    pub background_color_opt: Option<YamlColor>,
    #[validate]
    #[serde(rename = "backgroundOnColor")]
    pub background_on_color_opt: Option<YamlColor>,
    #[validate]
    #[serde(rename = "font")]
    pub font_opt: Option<YamlFont>,
    #[validate]
    #[serde(rename = "onFont")]
    pub on_font_opt: Option<YamlFont>,
    #[validate]
    #[serde(rename = "textColor")]
    pub text_color_opt: Option<YamlColor>,
    #[validate]
    #[serde(rename = "textOnColor")]
    pub text_on_color_opt: Option<YamlColor>,
    #[serde(rename = "zIndex")]
    pub z_index_opt: Option<isize>,
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::validate_ok;

    impl YamlVkCode {
        pub fn default_oneline_string() -> String {
            "{text: A, left: 50, top: 50}".to_string()
        }
    }

    fn parse(s: &str) -> YamlVkCode {
        serde_yaml::from_str::<YamlVkCode>(s).unwrap()
    }

    #[test]
    fn ok() {
        let v = parse(&YamlVkCode::default_oneline_string());
        validate_ok!(v);

        let v = parse(&format!(
            "
            text: B
            left: 0.0
            top: 0.0
            right: 100.0
            bottom: 50.0
            backgroundColor: {}
            backgroundOnColor: {}
            textColor: {}
            textOnColor: {}
            font: {}
            onFont: {}
            zIndex: -1
        ",
            YamlColor::default_oneline_string(),
            YamlColor::default_oneline_string(),
            YamlColor::default_oneline_string(),
            YamlColor::default_oneline_string(),
            YamlFont::default_oneline_string(),
            YamlFont::default_oneline_string(),
        ));
        validate_ok!(v);
    }
}
