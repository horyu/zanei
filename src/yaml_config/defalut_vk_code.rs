use super::YamlColor;
use super::YamlFont;

use serde::Deserialize;
use validator::Validate;

#[derive(Debug, Deserialize, Validate)]
pub struct YamlDefaultVkCode {
    pub width: f32,
    pub height: f32,
    #[validate]
    #[serde(rename = "backgroundColor")]
    pub background_color: YamlColor,
    #[validate]
    #[serde(rename = "backgroundOnColor")]
    pub background_on_color_opt: Option<YamlColor>,
    #[validate]
    #[serde(rename = "font")]
    pub font: YamlFont,
    #[validate]
    #[serde(rename = "onFont")]
    pub on_font_opt: Option<YamlFont>,
    #[validate]
    #[serde(rename = "textColor")]
    pub text_color: YamlColor,
    #[validate]
    #[serde(rename = "textOnColor")]
    pub text_on_color_opt: Option<YamlColor>,
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::validate_ok;

    impl YamlDefaultVkCode {
        pub fn default_oneline_string() -> String {
            format!(
                "{{width: 50, height: 50, backgroundColor: {}, font: {}, textColor: {}}}",
                YamlColor::default_oneline_string(),
                YamlFont::default_oneline_string(),
                YamlColor::default_oneline_string(),
            )
        }
    }

    fn parse(s: &str) -> YamlDefaultVkCode {
        serde_yaml::from_str::<YamlDefaultVkCode>(s).unwrap()
    }

    #[test]
    fn ok() {
        let v = parse(&YamlDefaultVkCode::default_oneline_string());
        validate_ok!(v);

        let v = parse(&format!(
            "
        width: 50
        height: 50
        backgroundColor: {}
        backgroundOnColor: {}
        font: {}
        onFont: {}
        textColor: {}
        textOnColor: {}
        ",
            YamlColor::default_oneline_string(),
            YamlColor::default_oneline_string(),
            YamlFont::default_oneline_string(),
            YamlFont::default_oneline_string(),
            YamlColor::default_oneline_string(),
            YamlColor::default_oneline_string(),
        ));
        validate_ok!(v);
    }
}
