use once_cell::sync::Lazy;
use regex::Regex;
use serde::Deserialize;
use validator::{Validate, ValidationError};

static RGB_TEXT_REGEX: Lazy<Regex> = Lazy::new(|| Regex::new(r##"^#?[0-9a-fA-F]{6}$"##).unwrap());

#[derive(Debug, Deserialize, Validate)]
#[validate(schema(function = "validate_yaml_color", skip_on_field_errors = false))]
pub struct YamlColor {
    #[validate(range(min = 0.0, max = 1.0))]
    #[serde(rename = "r")]
    pub r_opt: Option<f32>,
    #[validate(range(min = 0.0, max = 1.0))]
    #[serde(rename = "g")]
    pub g_opt: Option<f32>,
    #[validate(range(min = 0.0, max = 1.0))]
    #[serde(rename = "b")]
    pub b_opt: Option<f32>,
    #[validate(regex(
        path = "RGB_TEXT_REGEX",
        message = r"should match /^#?[0-9a-fA-F]{6}$/"
    ))]
    #[serde(rename = "rgb")]
    pub rgb_opt: Option<String>,
    #[validate(range(min = 0.0, max = 1.0))]
    #[serde(rename = "a")]
    pub a_opt: Option<f32>,
}

fn validate_yaml_color(yaml_color: &YamlColor) -> Result<(), ValidationError> {
    let rgb_opts_count = [yaml_color.r_opt, yaml_color.g_opt, yaml_color.b_opt]
        .iter()
        .filter(|opt| opt.is_some())
        .count();
    let has_rgb_text = yaml_color.rgb_opt.is_some();

    match (rgb_opts_count, has_rgb_text) {
        (3, false) | (0, true) => Ok(()),
        _ => {
            let mut e = ValidationError::new("color");
            e.message = Some("color requires [r,g,b] or [rgb].".into());
            Err(e)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{validate_err, validate_ok};

    impl YamlColor {
        pub fn default_oneline_string() -> String {
            "{r: 0, g: 0, b: 0}".to_string()
        }
    }

    fn parse(s: &str) -> YamlColor {
        serde_yaml::from_str::<YamlColor>(s).unwrap()
    }

    #[test]
    fn ok_rgb() {
        let c = parse(&YamlColor::default_oneline_string());
        validate_ok!(c);

        let c = parse(
            r##"
        r: -0.0
        g: 1.0
        b: 1e-9
        "##,
        );
        validate_ok!(c);

        let c = parse(
            r##"
        r: -0.0
        g: 1.0
        b: 1e-9
        a: 1.0
        "##,
        );
        validate_ok!(c);
    }

    #[test]
    fn ok_rgb_text() {
        let c = parse(
            r##"
        rgb: "014589"
        "##,
        );
        validate_ok!(c);

        let c = parse(
            r##"
        rgb: "#abcdef"
        "##,
        );
        validate_ok!(c);

        let c = parse(
            r##"
        rgb: FEDCBA
        a: 0.0
        "##,
        );
        validate_ok!(c);
    }

    #[test]
    fn invalid_rgb_text() {
        let c = parse(
            r##"
        rgb: "0123456"
        "##,
        );
        validate_err!(c);

        let c = parse(
            r##"
        rgb: |-
            012345
            012345
        "##,
        );
        validate_err!(c);
    }

    #[test]
    fn invalid_combination() {
        let c = parse(
            r##"
        r: 0.0
        rgb: "000000"
        "##,
        );
        validate_err!(c);

        let c = parse(
            r##"
        r: 0.0
        g: 0.0
        rgb: "000000"
        "##,
        );
        validate_err!(c);

        let c = parse(
            r##"
        r: 0.0
        g: 0.0
        b: 0.0
        rgb: "000000"
        a: 0.0
        "##,
        );
        validate_err!(c);
    }
}
