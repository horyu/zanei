use serde::Deserialize;
use validator::{Validate, ValidationError};

#[derive(Debug, Deserialize, Validate)]
pub struct YamlFont {
    #[validate(length(min = 1))]
    #[serde(rename = "fontName")]
    pub font_name: String,
    #[validate(custom = "validate_yaml_font_weight")]
    pub weight: i32,
    #[validate(range(
        min = 0,
        max = 2,
        code = "font",
        message = "invalid value. see https://docs.microsoft.com/en-us/windows/win32/api/dwrite/ne-dwrite-dwrite_font_style"
    ))]
    pub style: i32,
    #[validate(range(
        min = 0,
        max = 9,
        code = "font",
        message = "invalid value. see https://docs.microsoft.com/en-us/windows/win32/api/dwrite/ne-dwrite-dwrite_font_stretch"
    ))]
    pub stretch: i32,
    #[validate(range(min = 0.0))]
    pub size: f32,
}

fn validate_yaml_font_weight(weight: i32) -> Result<(), ValidationError> {
    if matches!(
        weight,
        100 | 200 | 300 | 350 | 400 | 500 | 600 | 700 | 800 | 900 | 950
    ) {
        Ok(())
    } else {
        let mut e = ValidationError::new("font");
        e.message = Some("invalid value. see https://docs.microsoft.com/en-us/windows/win32/api/dwrite/ne-dwrite-dwrite_font_weight".into());
        Err(e)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{validate_err, validate_ok};

    impl YamlFont {
        pub fn default_oneline_string() -> String {
            "{fontName: Segoe UI, weight: 100, style: 0, stretch: 0, size: 20}".to_string()
        }
    }

    fn parse(s: &str) -> YamlFont {
        serde_yaml::from_str::<YamlFont>(s).unwrap()
    }

    #[test]
    fn ok() {
        let f = parse(&YamlFont::default_oneline_string());
        validate_ok!(f);

        let f = parse(
            "
        fontName: Segoe UI
        weight: 400
        style: 0
        stretch: 0
        size: 20
        ",
        );
        validate_ok!(f);
    }

    #[test]
    fn invalid_wight() {
        let f = parse(
            "
        fontName: Segoe UI
        weight: 1
        style: 0
        stretch: 0
        size: 20
        ",
        );
        validate_err!(&f);
    }
}
